package com.nathap.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.nathap.dao.JobDAO;
import com.nathap.dto.JobcodeDTO;

public class JobListServlet extends HttpServlet{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		
		/*Job List*/
		
		JobDAO jobDAO = new JobDAO();
		List<JobcodeDTO> list = new ArrayList<JobcodeDTO>();
		list = jobDAO.getJobList();
		for(JobcodeDTO jb : list) {
			System.out.println(jb.getJobcode()+" "+jb.getJobtitle()+" "+jb.getDescription()+" "+jb.getLastdate()+" "+jb.getNoofpost());
			
		}
		
		req.setAttribute("jobList", list);
		 RequestDispatcher dispatcher = req.getServletContext().getRequestDispatcher("/jobs.jsp");
		 dispatcher.forward(req, res);
	}
	
	@Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        doGet(req, res);
    }

}
