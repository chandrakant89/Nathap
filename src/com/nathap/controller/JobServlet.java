package com.nathap.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.nathap.captcha.VerifyRecaptcha;
import com.nathap.dao.JobDAO;
import com.nathap.dto.JobDTO;
import com.nathap.dto.JobcodeDTO;
import com.nathap.mailer.Mailer;

@MultipartConfig(fileSizeThreshold=1024*1024*160, // 2MB
				 maxFileSize=1024*1024*5,       // 10MB
                 maxRequestSize=1024*1024*10)	// 20MB
public class JobServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		
	/*Job List*/
		
		JobDAO jobDAO = new JobDAO();
		List<JobcodeDTO> list = new ArrayList<JobcodeDTO>();
		list = jobDAO.getJobList();
		for(JobcodeDTO jb : list) {
			System.out.println(jb.getJobcode()+" "+jb.getJobtitle()+" "+jb.getDescription()+" "+jb.getLastdate()+" "+jb.getNoofpost());
			
		}
		
		req.setAttribute("jobList", list);
		 RequestDispatcher dispatcher = req.getServletContext().getRequestDispatcher("/jobs.jsp");
		 dispatcher.forward(req, res);
		
	}

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		JobDAO jobDAO = new JobDAO();
		String fname = req.getParameter("fname");
		String lname = req.getParameter("lname");
		String email = req.getParameter("email");
		int age = Integer.parseInt(req.getParameter("age"));
		String city = req.getParameter("city");
		String position = req.getParameter("position");
		String mob = req.getParameter("mob");
		String jobtype = req.getParameter("jobtype");
		String experience = req.getParameter("experience");
		String application = req.getParameter("application");
		String jobcode = req.getParameter("jobcode");
		String gRecaptchaResponse = req.getParameter("g-recaptcha-response");
		Part part = req.getPart("file");
		String fileName=extractFileName(part);
		//System.out.println("File "+part);
		String fullname =  fname +" "+ lname;
		
		String cc ="hr@nathap.in";
		
		String t = "The content of this message is confidential.If you have received it by mistake, please inform us by an email reply and then delete the message. It is forbidden to copy, forward, or in any way reveal the contents of this message to anyone. The integrity and security of this email cannot be guaranteed over the Internet. Therefore, the sender will not be held liable for any damage caused by the message.";
		String message ="<html><head><style type="+"text/css"+">"	
				+".d{ font-family: 'Lato', Tahoma, Verdana, Segoe, sans-serif; margin: 0 auto; max-width: 650px; color: #555;}"
				+".d1{height: 20px;}"
				+".d2{padding: 20px;}"
				+".d2 .img{text-align: center; border-bottom: 1px solid #555; padding: 10px;}"
				+".d3{text-align: center; padding: 30px; font-size: 18px;}"
				+".d5{padding: 30px; margin-top: 70px;}"
				+".d55{padding: 40px 0; border-top: 1px solid #ccc; border-bottom: 1px solid #ccc;}"
				+".d4{padding: 20px; font-size: 11px;}"
				+".d55 div{width: 25%; float: left; text-align: center; font-size: 11px;}"
				+"p{font-size: 13px;}"
				+"</style></head><body>"
				+"<div class="+"d"+">"
				+"<div class="+"d2"+"><div class="+"img"+"><a href="+"https://nathap.in/"+"><img src="+"https://nathap.in/images/logo.png"+"></a></div>"
				+"<div class="+"d3"+">Application Submitted Successfully</div>"
				+"<p>Hi "+fullname+"</p>"
				+"<p>Congratulations! your application for "+position+" has been submitted successfully. We will get you soon. </p>"
				+"</div>"
				+"<div class="+"d5"+"><div class="+"d55"+"><div>linkedin</div><div>twitter</div><div>facebook</div><div>youtube</div></div></div>"
				+"<div class="+"d4"+">"
				+"<div>Copyrights � 2018 All Rights Reserved.</div>"
				+"<div>You subscribeed to our newsletter via our website, nathap.in</div>"
				+"</div><div class="+"d4"+">"+t+"</div></div></body></html>";

			
			
			JobDTO jobDTO = new JobDTO();
			
			jobDTO.setFname(fname);
			jobDTO.setLname(lname);
			jobDTO.setEmail(email);
			jobDTO.setAge(age);
			jobDTO.setCity(city);
			jobDTO.setPosition(position);
			jobDTO.setMob(mob);
			jobDTO.setJobtype(jobtype);
			jobDTO.setExperience(experience);
			jobDTO.setApplication(application);
			jobDTO.setJobcode(jobcode);
			jobDTO.setgRecaptchaResponse(gRecaptchaResponse);
			jobDTO.setFile(part);
			jobDTO.setFilename(fileName);

			
			if(gRecaptchaResponse == "" || gRecaptchaResponse == null) {
				req.setAttribute("message", "Please Select Captcha");
				req.setAttribute("cls", "style-msg errormsg text-center");
				req.setAttribute("sbmsg", "sb-msg");
				req.getRequestDispatcher("jobListServlet").forward(req, res);
			}else {
				

			boolean verify = VerifyRecaptcha.verify(gRecaptchaResponse);
			if(verify){
			boolean jobRegistered = jobDAO.registerJob(jobDTO);
			if (jobRegistered) {
				req.setAttribute(
						"message",
						"Your information has been successfully submitted, We'll get back to you within 48 hours.");
				Mailer.sendMail(email, cc, "Hi "+fullname+" Your Application Submitted Successfully", message);
				req.setAttribute("cls", "style-msg successmsg text-center");
				req.setAttribute("sbmsg", "sb-msg");
				req.getRequestDispatcher("jobListServlet").forward(req, res);
			} else {
				req.setAttribute("message", "Opps...... Something is wrong");
				req.setAttribute("cls", "style-msg errormsg text-center");
				req.setAttribute("sbmsg", "sb-msg");
				req.getRequestDispatcher("jobListServlet").forward(req, res);
			}
				
			}else{
				
				req.setAttribute("message", "Opps...... Captcha is wrong");
				req.setAttribute("cls", "style-msg errormsg text-center");
				req.setAttribute("sbmsg", "sb-msg");
				req.getRequestDispatcher("jobListServlet").forward(req, res);
				
			}
		}
		

	}
	
	private String extractFileName(Part part) {
        String contentDisp = part.getHeader("content-disposition");
        String[] items = contentDisp.split(";");
        for (String s : items) {
            if (s.trim().startsWith("filename")) {
                return s.substring(s.indexOf("=") + 2, s.length()-1);
            }
        }
        return "";
    }

}
