package com.nathap.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.nathap.captcha.VerifyRecaptcha;
import com.nathap.dao.JobDAO;
import com.nathap.dto.JobSkillsDTO;

@MultipartConfig(fileSizeThreshold=1024*1024*2, // 2MB
				 maxFileSize=1024*1024*10,      // 10MB
				 maxRequestSize=1024*1024*50)	// 50MB
public class SkillServlet extends HttpServlet {
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
	
	}
	
	
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		System.out.println("Skill Submmition");
		JobDAO jobDAO = new JobDAO();
		//String skillid = req.getParameter("skillid");
		String fname = req.getParameter("fname");
		String lname = req.getParameter("lname");
		String email = req.getParameter("email");
		String city = req.getParameter("city");
		String mob = req.getParameter("mob");
		String yourskills = req.getParameter("yourskills");
		String gRecaptchaResponse = req.getParameter("g-recaptcha-response");
		Part uploadfile = req.getPart("uploadfile");
		String fileName=extractFileName(uploadfile);
		
		JobSkillsDTO jobSkillsDTO = new JobSkillsDTO();
		
		jobSkillsDTO.setFname(fname);
		jobSkillsDTO.setLname(lname);
		jobSkillsDTO.setEmail(email);
		jobSkillsDTO.setCity(city);
		jobSkillsDTO.setMob(mob);
		jobSkillsDTO.setYourskills(yourskills);
		jobSkillsDTO.setgRecaptchaResponse(gRecaptchaResponse);
		jobSkillsDTO.setUploadfile(uploadfile);
		jobSkillsDTO.setFilename(fileName);
		
		if(gRecaptchaResponse == "" || gRecaptchaResponse == null) {
			req.setAttribute("message", "Please Select Captcha");
			req.setAttribute("cls", "style-msg errormsg text-center");
			req.setAttribute("sbmsg", "sb-msg");
			//res.sendRedirect("jobListServlet");
			req.getRequestDispatcher("jobListServlet").forward(req, res);
			/*req.getRequestDispatcher("/jobs.jsp").forward(req, res);*/	
		}else {

		boolean verify = VerifyRecaptcha.verify(gRecaptchaResponse);
		if(verify){
		boolean skillRegistered = jobDAO.postSkills(jobSkillsDTO);
		if (skillRegistered) {
			req.setAttribute(
					"message",
					"Your information has been successfully submitted.");
			req.setAttribute("cls", "style-msg successmsg text-center");
			req.setAttribute("sbmsg", "sb-msg");
			//res.sendRedirect("jobListServlet");
			req.getRequestDispatcher("jobListServlet").forward(req, res);
			//req.getRequestDispatcher("/jobs.jsp").forward(req, res);
		} else {
			req.setAttribute("message", "Opps...... Something is wrong");
			req.setAttribute("cls", "style-msg errormsg text-center");
			req.setAttribute("sbmsg", "sb-msg");
			//res.sendRedirect("jobListServlet");
			req.getRequestDispatcher("jobListServlet").forward(req, res);
			//req.getRequestDispatcher("/jobs.jsp").forward(req, res);
		}
			
		}else{
			
			req.setAttribute("message", "Opps...... Captcha is wrong");
			req.setAttribute("cls", "style-msg errormsg text-center");
			req.setAttribute("sbmsg", "sb-msg");
			//res.sendRedirect("jobListServlet");
			req.getRequestDispatcher("jobListServlet").forward(req, res);
			//req.getRequestDispatcher("/jobs.jsp").forward(req, res);
			
		}
	}
		
		
	}
	
	private String extractFileName(Part part) {
        String contentDisp = part.getHeader("content-disposition");
        String[] items = contentDisp.split(";");
        for (String s : items) {
            if (s.trim().startsWith("filename")) {
                return s.substring(s.indexOf("=") + 2, s.length()-1);
            }
        }
        return "";
    }

}
