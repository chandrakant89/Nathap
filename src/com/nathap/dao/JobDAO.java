
package com.nathap.dao;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.servlet.http.Part;

import com.nathap.dto.JobDTO;
import com.nathap.dto.JobSkillsDTO;
import com.nathap.dto.JobcodeDTO;
import com.nathap.util.DBConnection;

public class JobDAO {
	private static final String SAVE_DIR="files";
	public boolean registerJob(JobDTO jobDTO) throws IOException {
		
		boolean flag = false;
		InputStream inputStream = null;
		Date d = new Date();
		SimpleDateFormat dateformate = new SimpleDateFormat("yyyy-MM-dd");
		
			
		String savePath = "D:/" + SAVE_DIR;
		
		//String savePath = "/home/thenatur//" + SAVE_DIR;
		
        File fileSaveDir=new File(savePath);
        if(!fileSaveDir.exists()){
            fileSaveDir.mkdir();
        }
		
		String fname = jobDTO.getFname();
		String lname = jobDTO.getLname();
		String email = jobDTO.getEmail();
		int age 	 = jobDTO.getAge();
		String city  = jobDTO.getCity();
		String position  = jobDTO.getPosition();
		String mob = jobDTO.getMob();
		String jobtype = jobDTO.getJobtype();
		String date = dateformate.format(d);
		String experience   = jobDTO.getExperience();
		String application = jobDTO.getApplication();
		String jobcode = jobDTO.getJobcode();
		Part part = jobDTO.getFile();
		String filename = jobDTO.getFilename();
		long num = generateRandNumber();
		
		Connection con = null;
		PreparedStatement preparedStatement = null;
		 try {
			 con = DBConnection.createConnection();
			 String query = "insert into userdetail(userid,fname, lname, email, age, city, position, mobile, jobtype, date, experience, application, jobcode, filepath) "
			 		+ "values (NULL,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			 preparedStatement = con.prepareStatement(query);
			 preparedStatement.setString(1, fname);
			 preparedStatement.setString(2, lname);
			 preparedStatement.setString(3, email);
			 preparedStatement.setInt(4, age);
			 preparedStatement.setString(5, city);
			 preparedStatement.setString(6, position);
			 preparedStatement.setString(7, mob); 
			 preparedStatement.setString(8, jobtype);
			 preparedStatement.setString(9, date);
			 preparedStatement.setString(10, experience);
			 preparedStatement.setString(11, application);
			 preparedStatement.setString(12, jobcode);
			 String filepath= savePath + "/" + num + filename ;
			 preparedStatement.setString(13, filepath);
		
			 
			 int i= preparedStatement.executeUpdate();
			 if (i!=0) {
				 flag = true;
				 part.write(savePath + "/"+ num + filename);
				 return flag;
			 }else {
				 flag = false;
			 }
		 }catch(SQLException e) {
			 e.printStackTrace();
		 }catch(NullPointerException e) {
			 e.printStackTrace();
		 }
		finally {

			try {
				if (con != null && preparedStatement != null) {
					con.close();
					preparedStatement.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		 return flag;
		
	}
	
	
	/*Job List*/
	
	public List<JobcodeDTO> getJobList() {
		
		Connection con = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<JobcodeDTO> list=new ArrayList<JobcodeDTO>();  
		
		try {
			 con = DBConnection.createConnection();
			 String sql ="SELECT * FROM jobdetail";
			 preparedStatement = con.prepareStatement(sql);
			 resultSet = preparedStatement.executeQuery();
			 while(resultSet.next()){
				 JobcodeDTO jobcodeDTO = new JobcodeDTO();
				 jobcodeDTO.setJobcode(resultSet.getString(1));
				 jobcodeDTO.setJobtitle(resultSet.getString(2));
				 jobcodeDTO.setDescription(resultSet.getString(3));
				 jobcodeDTO.setLastdate(resultSet.getDate(4));
				 jobcodeDTO.setNoofpost(resultSet.getString(5));
				 
				 
				 list.add(jobcodeDTO);
			 }
			
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if (con != null && preparedStatement != null) {
					con.close();
					preparedStatement.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
		}
		
		return list;
	}
	
	
	/*Get Job By Id
		public JobcodeDTO getJobCodeById(int jobid) {
			System.out.println("Jobid "+jobid);
			Connection con = null;
			PreparedStatement preparedStatement = null;
			ResultSet resultSet = null;
			JobcodeDTO jobcodeDTO = new JobcodeDTO();
			try {
				 con = DBConnection.createConnection();
				 String sql ="select * from jobdetail where jobid=?";
				 preparedStatement = con.prepareStatement(sql);
				 preparedStatement.setInt(1, jobid);
				 resultSet = preparedStatement.executeQuery();
				 if(resultSet.next()){
					 jobcodeDTO.setJobid(resultSet.getString(1));
					 jobcodeDTO.setJobtitle(resultSet.getString(2));
					 jobcodeDTO.setDescription(resultSet.getString(3));
					
				 }
				
			}catch (Exception e) {
				e.printStackTrace();
			}finally {
				try {
					if (con != null && preparedStatement != null) {
						con.close();
						preparedStatement.close();
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
				
			}
			return jobcodeDTO;
			
		}*/
		
		//Post Your Skills
		public boolean postSkills(JobSkillsDTO jobSkillsDTO) throws IOException {
			boolean flag = false;
			
			InputStream inputStream = null;
			Date d = new Date();
			SimpleDateFormat dateformate = new SimpleDateFormat("yyyy-MM-dd");
			
			String savePath = "D:" + File.separator + SAVE_DIR;
	        File fileSaveDir=new File(savePath);
	        if(!fileSaveDir.exists()){
	            fileSaveDir.mkdir();
	        }
			
			String fname = jobSkillsDTO.getFname();
			String lname = jobSkillsDTO.getLname();
			String email = jobSkillsDTO.getEmail();
			String city  = jobSkillsDTO.getCity();
			String mob = jobSkillsDTO.getMob();
			String date = dateformate.format(d);
			String yourskills = jobSkillsDTO.getYourskills();
			Part uploadfile = jobSkillsDTO.getUploadfile();
			
			String filename = jobSkillsDTO.getFilename();
			long num = generateRandNumber();
			uploadfile.write(savePath + File.separator + num + filename);
		
			
			Connection con = null;
			PreparedStatement preparedStatement = null;
			 try {
				 con = DBConnection.createConnection();
				 String query = "insert into skills(skillid, fname, lname, email, city, mobile, date, yourskills, filepath) "
				 		+ "values (NULL,?,?,?,?,?,?,?,?)";
				 preparedStatement = con.prepareStatement(query);
				 
				 preparedStatement.setString(1, fname);
				 preparedStatement.setString(2, lname);
				 preparedStatement.setString(3, email);
				 preparedStatement.setString(4, city);
				 preparedStatement.setString(5, mob); 
				 preparedStatement.setString(6, date);
				 preparedStatement.setString(7, yourskills);
				 String filepath= savePath + File.separator + num + filename ;
				 preparedStatement.setString(8, filepath);
				 
				 int i= preparedStatement.executeUpdate();
				 if (i!=0) {
					 flag = true;
					 return flag;
				 }else {
					 flag = false;
				 }
			 }catch(SQLException e) {
				 e.printStackTrace();
			 }catch(NullPointerException e) {
				 e.printStackTrace();
			 }
			finally {

				try {
					if (con != null && preparedStatement != null) {
						con.close();
						preparedStatement.close();
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			 return flag;
			
		}
		
		public static long generateRandNumber(){
			long num=0;
			final Random rn = new Random();
			
			int number = 0;

			for (int i = 0; i < 3; i++) {
				
				number = rn.nextInt();
				num = (num + (number < 0 ? -1 * number : number));
			}
			return num;
		}
	

}