package com.nathap.dto;

import javax.servlet.http.Part;

public class JobDTO {
	private int userid;
	private String fname;
	private String lname;
	private String email;
	private int age;
	private String city;
	private String position;
	private String mob;
	private String jobtype;
	private String startdate;
	private String experience;
	private String application;
	private String jobcode;
	private String gRecaptchaResponse;
	/*Part filepart;*/
	private Part file;
	private String filename;
	
	public JobDTO() {
		}
	
	
	public JobDTO(int userid, String fname, String lname, String email, int age, String city, String position, String mob, String jobtype,
			String startdate, String experience, String application, String jobcode, String gRecaptchaResponse, Part file, String filename) {
		super();
		this.userid = userid;
		this.fname = fname;
		this.lname = lname;
		this.email = email;
		this.age = age;
		this.city = city;
		this.position = position;
		this.mob = mob;
		this.jobtype = jobtype;
		this.startdate = startdate;
		this.experience = experience;
		this.application = application;
		this.jobcode = jobcode;
		this.gRecaptchaResponse = gRecaptchaResponse;
		this.file = file;
		this.filename = filename;
	}


	public int getUserid() {
		return userid;
	}


	public void setUserid(int userid) {
		this.userid = userid;
	}


	public String getFname() {
		return fname;
	}


	public void setFname(String fname) {
		this.fname = fname;
	}


	public String getLname() {
		return lname;
	}


	public void setLname(String lname) {
		this.lname = lname;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public int getAge() {
		return age;
	}


	public void setAge(int age) {
		this.age = age;
	}


	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public String getPosition() {
		return position;
	}


	public void setPosition(String position) {
		this.position = position;
	}


	public String getMob() {
		return mob;
	}


	public void setMob(String mob) {
		this.mob = mob;
	}


	public String getJobtype() {
		return jobtype;
	}


	public void setJobtype(String jobtype) {
		this.jobtype = jobtype;
	}


	public String getStartdate() {
		return startdate;
	}


	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}


	public String getExperience() {
		return experience;
	}


	public void setExperience(String experience) {
		this.experience = experience;
	}


	public String getApplication() {
		return application;
	}


	public void setApplication(String application) {
		this.application = application;
	}


	public String getJobcode() {
		return jobcode;
	}


	public void setJobcode(String jobcode) {
		this.jobcode = jobcode;
	}


	public String getgRecaptchaResponse() {
		return gRecaptchaResponse;
	}


	public void setgRecaptchaResponse(String gRecaptchaResponse) {
		this.gRecaptchaResponse = gRecaptchaResponse;
	}


	public Part getFile() {
		return file;
	}


	public void setFile(Part file) {
		this.file = file;
	}


	public String getFilename() {
		return filename;
	}


	public void setFilename(String filename) {
		this.filename = filename;
	}


}
