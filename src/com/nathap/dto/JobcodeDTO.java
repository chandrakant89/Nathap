package com.nathap.dto;

import java.util.Date;

public class JobcodeDTO {
	private String jobcode;
	private String jobtitle;
	private String description;
	private Date lastdate;
	private String noofpost;
	
	public JobcodeDTO() {
		super();
	}

	public JobcodeDTO(String jobcode, String jobtitle, String description, Date lastdate, String noofpost) {
		super();
		this.jobcode = jobcode;
		this.jobtitle = jobtitle;
		this.description = description;
		this.lastdate = lastdate;
		this.noofpost = noofpost;
	}


	public String getJobcode() {
		return jobcode;
	}

	public void setJobcode(String jobcode) {
		this.jobcode = jobcode;
	}

	public String getJobtitle() {
		return jobtitle;
	}


	public void setJobtitle(String jobtitle) {
		this.jobtitle = jobtitle;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}

	public Date getLastdate() {
		return lastdate;
	}

	public void setLastdate(Date lastdate) {
		this.lastdate = lastdate;
	}

	public String getNoofpost() {
		return noofpost;
	}

	public void setNoofpost(String noofpost) {
		this.noofpost = noofpost;
	}
	
	
}
