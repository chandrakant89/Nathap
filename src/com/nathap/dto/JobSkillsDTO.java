package com.nathap.dto;

import javax.servlet.http.Part;

public class JobSkillsDTO {
	private int skillid;
	private String fname;
	private String lname;
	private String email;
	private String city;
	private String mob;
	private String yourskills;
	private String gRecaptchaResponse;
	private Part uploadfile;
	private String filename;
	
	public JobSkillsDTO() {
		super();
	}


	public JobSkillsDTO(int skillid, String fname, String lname, String email, String city, String mob, Part uploadfile, String filename,
			String yourskills, String gRecaptchaResponse) {
		super();
		this.skillid = skillid;
		this.fname = fname;
		this.lname = lname;
		this.email = email;
		this.city = city;
		this.mob = mob;
		this.yourskills = yourskills;
		this.gRecaptchaResponse = gRecaptchaResponse;
		this.uploadfile = uploadfile;
		this.filename = filename;
	}


	public int getSkillid() {
		return skillid;
	}


	public void setSkillid(int skillid) {
		this.skillid = skillid;
	}


	public String getFname() {
		return fname;
	}


	public void setFname(String fname) {
		this.fname = fname;
	}


	public String getLname() {
		return lname;
	}


	public void setLname(String lname) {
		this.lname = lname;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public String getMob() {
		return mob;
	}


	public void setMob(String mob) {
		this.mob = mob;
	}


	public String getYourskills() {
		return yourskills;
	}


	public void setYourskills(String yourskills) {
		this.yourskills = yourskills;
	}


	public String getgRecaptchaResponse() {
		return gRecaptchaResponse;
	}


	public void setgRecaptchaResponse(String gRecaptchaResponse) {
		this.gRecaptchaResponse = gRecaptchaResponse;
	}


	public Part getUploadfile() {
		return uploadfile;
	}


	public void setUploadfile(Part uploadfile) {
		this.uploadfile = uploadfile;
	}


	public String getFilename() {
		return filename;
	}


	public void setFilename(String filename) {
		this.filename = filename;
	}
	
	

}
