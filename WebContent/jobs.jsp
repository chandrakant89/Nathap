
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@include file="webResource.jsp" %>
<%@include file="header.jsp" %>
<script src='https://www.google.com/recaptcha/api.js'></script>
					
		<section id="page-title" class="page-title-parallax page-title-dark" style="background-image: url('images/about/1.jpg'); padding: 120px 0; background-position: center; background-size: cover;" data-stellar-background-ratio="0.3">

			<div class="container clearfix">
				<div class="col-md-9">
					<h1>Job Openings</h1>
					<span>Join our Fabulous Team of Intelligent Individuals</span>
				</div>
				<div class="col-md-2 button_blink text-center">
					<div>
						<a style="color:#fff" href="https://nathap.in/Admin">HR Login</a>
					</div>
				</div>
			</div>
			

		</section>

		<section id="content">

			<div class="content-wrap">

				<div class="container clearfix">
					<div class="${cls}">
						<div class="${sbmsg}"><strong>${message}</strong></div>
					</div>	
					<div class="">
					
							<c:forEach items="${jobList}" var="jlist" >
								<div class="col-md-6 col-sm-12">
								<div class="">
								<div id="jobs_l" class="jobs_list">
									<div class="jobs_title">
										<span class="jobCode" id="jobCode">${jlist.jobcode}</span>
										<span id="jobTitle"><i class="icon-trophy"></i> ${jlist.jobtitle}</span>
									</div>
									<div class="decrip" id="${jlist.jobcode}">
										<span class="label_">Description:</span>
											<div class="desc">${jlist.description}</div>
										<span class="label_"><i class="icon-calendar3"></i> Last date:</span>
											<div class="desc">${jlist.lastdate}</div>
										<span class="label_">No of post:</span>
											<div class="desc">${jlist.noofpost}</div>	
										<span class="label_"></span>
											<div class="appBtn">
												<a class="button button-3d button-mini button-rounded button-dirtygreen" 
												data-toggle="modal" data-target=".bs-example-modal-lg" 
												id="apply" onclick="applyJob('${jlist.jobtitle}','${jlist.jobcode}')">Apply Now</a>
											</div>		
									</div>
								</div>
								</div>
						</div>
							</c:forEach>
				  
				  
				</div>
				

			</div>

			</div>

		</section><!-- #content end -->

<!-- popup -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-body">
				<div class="modal-content">
					<div class="modal-body">
						<!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> -->
						<div class="jobForm">
						<div class="col-md-12">
							<div class="row">
							<div style="float: left"><h4 class=""><span id="jTitle"></span></h4></div> 
							<div style="float: right;"><h4 class=""><span>JOB CODE :</span> <span id="jobcode1"></span></h4></div> 
							</div>
						</div>
						<form action="jobServlet" id="jobForm" name="jobForm" method="post" enctype="multipart/form-data">
						<input type="hidden" name="jobcode" id="jobcode" value="" class="sm-form-control" />
							<div class="col_half">
								<label for="template-jobform-fname">First Name <small>*</small></label>
								<input type="text" name="fname" id="fname" value="" class="sm-form-control" required maxlength="45"/>
								<p class="text-danger" id="fnameError"></p>
							</div>

							<div class="col_half col_last">
								<label for="template-jobform-lname">Last Name <small>*</small></label>
								<input type="text" name="lname" id="lname" value="" class="sm-form-control" required maxlength="45"/>
								<p class="text-danger" id="lnameError"></p>
							</div>

							<div class="col_full">
								<label for="template-jobform-email">Email <small>*</small></label>
								<input type="email" id="email" name="email" value="" class="email sm-form-control" required maxlength="45"/>
								<p class="text-danger" id="emailError"></p>
							</div>
							
							<div class="col_half">
								<label for="template-jobform-age">Age <small>*</small></label>
								<input type="text" name="age" id="age" value="" onkeypress="javascript:return isNumber(event)" maxlength="2" class="sm-form-control" required/>
								<p class="text-danger" id="ageError"></p>
							</div>

							<div class="col_half col_last">
								<label for="template-jobform-city">City <small>*</small></label>
								<input type="text" name="city" id="city" value="" class="sm-form-control" required maxlength="45"/>
								<p class="text-danger" id="cityError"></p>
							</div>

							<div class="col_half">
								<label for="template-jobform-service">Position <small>*</small></label>
								<input type="text" name="position" id="position" value="" class="sm-form-control" required readonly="readonly"/>
							</div>
							
							<div class="col_half col_last">
								<label for="template-jobform-email">Mobile No. <small>*</small></label>
								<input type="text" id="mob" name="mob" onkeypress="javascript:return isNumberMob(event)"  maxlength="10" value="" class="sm-form-control" required/>
								<p class="text-danger" id="mobError"></p>
							</div>

							<div class="col_half">
								<label for="template-jobform-salary">Job Type</label>
								<select name="jobtype" id="jobtype" class="sm-form-control" required>
									<option value="">--select Job Type --</option>
									<option value="fullTime">Full Time</option>
									<option value="partTime">Part Time</option>
								</select>
								<p class="text-danger" id="jobtError"></p>
							</div>
							
							<div class="col_half col_last">
								<label for="template-jobform-experience">Experience (optional)</label>
								<select name="experience" id="experience" class="sm-form-control" required>
									<option value="">--select experience --</option>
									<option value="0">0 Year </option>
									<option value="<1"> < 1 Year </option>
									<option value="1"> 1 Year </option>
									<option value="2"> 2 Year </option>
									<option value="3"> 3 Year </option>
									<option value="4"> 4 Year </option>
									<option value="5"> 5 Year </option>
									<option value=">5"> > 5 Year </option>
								</select>
								<p class="text-danger" id="exepiError"></p>
							</div>

							<div class="col_full">
								<label for="template-jobform-application">Remark <small>*</small></label>
								<textarea name="application" id="application" rows="6" maxlength="250" class="sm-form-control" required></textarea>
								<p class="text-danger" id="remarkError"></p>
							</div>
							
							<div class="col_half">
								<label for="template-jobform-application">Upload Resume <small>*</small></label>
								<input type="file" name="file" id = "file" required style="box-shadow: 5px 7px 7px 1px transparent !important; border: 1px solid transparent !important;"/>
								<p class="text-danger" id="fileError"></p>
								<p>Supported Formats: doc, docx, pdf, upto 10 MB </p>
							</div>
							
							 <!-- For localhost site key -->
							
							<div class="col_half col_last ">
								<div  class="g-recaptcha" data-sitekey="6LfC_lwUAAAAAPQOKDIuX57NGA3S0UXTJrE9LnU2" data-callback="YourOnSubmitFn"></div>
								<p class="text-danger" id="captcha"></p>
							</div>
							
							<!-- for live project -->
							<!-- <div class="col_half col_last">
								<div class="g-recaptcha" data-sitekey="6LdARV4UAAAAANErMMto39Ar9KINk4NZoc1DSNd1" data-callback="YourOnSubmitFn"></div>
								<p class="text-danger" id="captcha"></p>
							</div>   -->
							
							
							<div class="col_full text-center">
								<input class="button button-3d nomargin" id="submit" type="submit" value="Send Application" onclick="return ValidateExtensionApplyJob()"/>
							</div>
						</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<%@include file="footer.jsp" %>
<%@include file="webend.jsp" %>
