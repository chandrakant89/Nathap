<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@include file="webResource.jsp"%>
<%@include file="header.jsp"%>

<section id="page-title" class="page-title-parallax page-title-dark"
	style="background-image: url('images/about/3.jpg'); padding: 120px 0; background-position: center; background-size: cover;"
	data-stellar-background-ratio="0.3">

	<div class="container clearfix">
		<h1>About the Nathap</h1>
		<span>The Birth of Nat Hap Group is Only for Growth of
			Bundelkhand (UP). Bundelkhand (UP) is a region and also a mountain
			range in central India. </span>
	</div>
</section>

<section id="content">

	<div class="content-wrap" style="background-color: #eee">

		<div class="container clearfix">

			<div class="col_one_third">

				<div
					class="heading-block fancy-title nobottomborder title-bottom-border">
					<h4>
						Why choose <span>Us</span>.
					</h4>
				</div>

				<p>Our future resolution motivates us to advance development.
					Development is the most important issue in the country, and the
					relation of development of the country is directly related to the
					development of each individual. NAT HAP is committed to the
					development of each individual.</p>

			</div>

			<div class="col_one_third">

				<div
					class="heading-block fancy-title nobottomborder title-bottom-border">
					<h4>
						Our <span>Mission</span>.
					</h4>
				</div>

				<p>NAT HAP is moving forward towards advanced development whose
					purpose is to add end-person of country to the latest technology in
					daily life, to generate employment, Availability of quality
					education and ensuring research on it, Research on Agriculture
					based comprehensive, marketing.</p>

			</div>

			<div class="col_one_third col_last">

				<div
					class="heading-block fancy-title nobottomborder title-bottom-border">
					<h4>
						What we <span>Do</span>.
					</h4>
				</div>

				<p>NAT HAP Group is an attempt to make some revolutionary youth
					whose dream is to develop Bundelkhand (UP) in every sphere. People
					of Bundelkhand (UP), who have been dying of drought for the last
					several years, are being forced to flee.</p>

			</div>

		</div>
	</div>
	<div style="margin-top: 60px;">
		<div class="container clearfix">

			<div class="row topmargin-sm">

				<div class="heading-block center">
					<h3>Meet Our Team</h3>
				</div>

				<div class="col-md-2 col-sm-4 bottommargin">
					<div class="team">
						<div class="team-image">
							<img src="images/team/1.jpg" alt="">
						</div>
						<div class="team-desc team-desc-bg">
							<div class="team-title">
								<h4>John Doe</h4>
								<span>CEO</span>
							</div>
							<a href="#"
								class="social-icon inline-block si-small si-light si-rounded si-facebook">
								<i class="icon-facebook"></i> <i class="icon-facebook"></i>
							</a> <a href="#"
								class="social-icon inline-block si-small si-light si-rounded si-twitter">
								<i class="icon-twitter"></i> <i class="icon-twitter"></i>
							</a> <a href="#"
								class="social-icon inline-block si-small si-light si-rounded si-gplus">
								<i class="icon-gplus"></i> <i class="icon-gplus"></i>
							</a>
						</div>
					</div>
				</div>

				<div class="col-md-2 col-sm-4 bottommargin">
					<div class="team">
						<div class="team-image">
							<img src="images/team/1.jpg" alt="">
						</div>
						<div class="team-desc team-desc-bg">
							<div class="team-title">
								<h4>Josh Clark</h4>
								<span>Co-Founder</span>
							</div>
							<a href="#"
								class="social-icon inline-block si-small si-light si-rounded si-facebook">
								<i class="icon-facebook"></i> <i class="icon-facebook"></i>
							</a> <a href="#"
								class="social-icon inline-block si-small si-light si-rounded si-twitter">
								<i class="icon-twitter"></i> <i class="icon-twitter"></i>
							</a> <a href="#"
								class="social-icon inline-block si-small si-light si-rounded si-gplus">
								<i class="icon-gplus"></i> <i class="icon-gplus"></i>
							</a>
						</div>
					</div>
				</div>

				<div class="col-md-2 col-sm-4 bottommargin">
					<div class="team">
						<div class="team-image">
							<img src="images/team/1.jpg" alt="">
						</div>
						<div class="team-desc team-desc-bg">
							<div class="team-title">
								<h4>Mary Jane</h4>
								<span>Sales</span>
							</div>
							<a href="#"
								class="social-icon inline-block si-small si-light si-rounded si-facebook">
								<i class="icon-facebook"></i> <i class="icon-facebook"></i>
							</a> <a href="#"
								class="social-icon inline-block si-small si-light si-rounded si-twitter">
								<i class="icon-twitter"></i> <i class="icon-twitter"></i>
							</a> <a href="#"
								class="social-icon inline-block si-small si-light si-rounded si-gplus">
								<i class="icon-gplus"></i> <i class="icon-gplus"></i>
							</a>
						</div>
					</div>
				</div>

				<div class="col-md-2 col-sm-4 bottommargin">
					<div class="team">
						<div class="team-image">
							<img src="images/team/1.jpg" alt="">
						</div>
						<div class="team-desc team-desc-bg">
							<div class="team-title">
								<h4>Nix Maxwell</h4>
								<span>Support</span>
							</div>
							<a href="#"
								class="social-icon inline-block si-small si-light si-rounded si-facebook">
								<i class="icon-facebook"></i> <i class="icon-facebook"></i>
							</a> <a href="#"
								class="social-icon inline-block si-small si-light si-rounded si-twitter">
								<i class="icon-twitter"></i> <i class="icon-twitter"></i>
							</a> <a href="#"
								class="social-icon inline-block si-small si-light si-rounded si-gplus">
								<i class="icon-gplus"></i> <i class="icon-gplus"></i>
							</a>
						</div>
					</div>
				</div>

				<div class="col-md-2 col-sm-4 bottommargin">
					<div class="team">
						<div class="team-image">
							<img src="images/team/1.jpg" alt="">
						</div>
						<div class="team-desc team-desc-bg">
							<div class="team-title">
								<h4>Nix Maxwell</h4>
								<span>Support</span>
							</div>
							<a href="#"
								class="social-icon inline-block si-small si-light si-rounded si-facebook">
								<i class="icon-facebook"></i> <i class="icon-facebook"></i>
							</a> <a href="#"
								class="social-icon inline-block si-small si-light si-rounded si-twitter">
								<i class="icon-twitter"></i> <i class="icon-twitter"></i>
							</a> <a href="#"
								class="social-icon inline-block si-small si-light si-rounded si-gplus">
								<i class="icon-gplus"></i> <i class="icon-gplus"></i>
							</a>
						</div>
					</div>
				</div>

				<div class="col-md-2 col-sm-4 bottommargin">
					<div class="team">
						<div class="team-image">
							<img src="images/team/1.jpg" alt="">
						</div>
						<div class="team-desc team-desc-bg">
							<div class="team-title">
								<h4>Nix Maxwell</h4>
								<span>Support</span>
							</div>
							<a href="#"
								class="social-icon inline-block si-small si-light si-rounded si-facebook">
								<i class="icon-facebook"></i> <i class="icon-facebook"></i>
							</a> <a href="#"
								class="social-icon inline-block si-small si-light si-rounded si-twitter">
								<i class="icon-twitter"></i> <i class="icon-twitter"></i>
							</a> <a href="#"
								class="social-icon inline-block si-small si-light si-rounded si-gplus">
								<i class="icon-gplus"></i> <i class="icon-gplus"></i>
							</a>
						</div>
					</div>
				</div>

			</div>

		</div>
	</div>
	<div class="section footer-stick nobottommargin">
		<h3 class="uppercase center">
			OUR <span>Ideals</span>
		</h3>
		<div class="fslider testimonial testimonial-full"
			data-animation="fade" data-arrows="false">
			<div class="flexslider">
				<div class="slider-wrap">
					<div class="slide">
						<div class="testi-image">
							<a href="#"><img src="images/testimonials/1.jpg"
								alt="Customer Testimonails"></a>
						</div>
						<div class="testi-content">
							<p>Take up one idea. Make that one idea your life - think of
								it, the dream of it, live on that idea. Let the brain, muscles,
								nerves, every part of your body, be full of that idea, and just
								leave every other idea alone. This is the way to success</p>
							<div class="testi-meta">Swami Vivekananda</div>
						</div>
					</div>
					<div class="slide">
						<div class="testi-image">
							<a href="#"><img src="images/testimonials/2.jpg"
								alt="Customer Testimonails"></a>
						</div>
						<div class="testi-content">
							<p>Never stop fighting until you arrive at your destined
								place - that is, the unique you. Have an aim in life,
								continuously acquire knowledge, work hard, and have the
								perseverance to realize the great life.</p>
							<div class="testi-meta">A.P. J. Abdul Kalam</div>
						</div>
					</div>
					<div class="slide">
						<div class="testi-image">
							<a href="#"><img src="images/testimonials/3.jpg"
								alt="Customer Testimonails"></a>
						</div>
						<div class="testi-content">
							<p>Never stop fighting until you arrive at your destined
								place - that is, the unique you. Have an aim in life,
								continuously acquire knowledge, work hard, and have the
								perseverance to realize the great life.</p>
							<div class="testi-meta">Steve Jobs</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
	<div id="content">
		<div class="container clearfix" style="margin-top: 60px; margin-bottom: 60px;">
		<div
			class="heading-block fancy-title nobottomborder title-bottom-border">
			<h4>
				Our <span>Client</span>.
			</h4>
		</div>
		<div id="oc-clients"
			class="owl-carousel image-carousel carousel-widget" data-margin="60"
			data-loop="true" data-nav="false" data-autoplay="4000"
			data-pagi="false" data-items-xxs="2" data-items-xs="3"
			data-items-sm="4" data-items-md="5" data-items-lg="6">
			<div class="oc-item">
				<a href="#"><img src="images/clients/1.png" alt="Clients"></a>
			</div>
			<div class="oc-item">
				<a href="#"><img src="images/clients/2.png" alt="Clients"></a>
			</div>
			<div class="oc-item">
				<a href="#"><img src="images/clients/3.png" alt="Clients"></a>
			</div>
			<div class="oc-item">
				<a href="#"><img src="images/clients/4.png" alt="Clients"></a>
			</div>
			<div class="oc-item">
				<a href="#"><img src="images/clients/5.png" alt="Clients"></a>
			</div>
		</div>
	</div>
	</div>



</section>
<!-- #content end -->

<%@include file="footer.jsp"%>
<%@include file="webend.jsp"%>