<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@include file="webResource.jsp"%>
<%@include file="header.jsp"%>

<section id="page-title" class="page-title-parallax page-title-dark"
	style="background-image: url('images/about/2.jpg'); padding: 120px 0; background-position: center; background-size: cover;"
	data-stellar-background-ratio="0.3">

	<div class="container clearfix">
		<h1>Contact Us</h1>
	</div>

</section>
    <section id="content">

			<div class="content-wrap">

				<div class="container clearfix">
					<div class="col_full col_last">
						<section id="map" class="gmap" style="height: 410px;"></section>
					</div><!-- Google Map End -->

					<div class="clear"></div>

					<!-- Contact Info
					============================================= -->
					<div class="row clear-bottommargin">

						<div class="col-md-3 col-sm-6 bottommargin clearfix">
							<div class="feature-box fbox-center fbox-bg fbox-plain">
								<div class="fbox-icon">
									<a href=""><i class="icon-map-marker"></i></a>
								</div>
								<h3>Our Headquarters<span class="subtitle">M-61A Janakpuri New Delhi</span></h3>
							</div>
						</div>

						<div class="col-md-3 col-sm-6 bottommargin clearfix">
							<div class="feature-box fbox-center fbox-bg fbox-plain">
								<div class="fbox-icon">
									<a href=""><i class="icon-phone3"></i></a>
								</div>
								<h3>Speak to Us<span class="subtitle"> (+91) 912 516 5305</span></h3>
							</div>
						</div>

						<div class="col-md-3 col-sm-6 bottommargin clearfix">
							<div class="feature-box fbox-center fbox-bg fbox-plain">
								<div class="fbox-icon">
									<a href=""><i class="icon-envelope-alt"></i></a>
								</div>
								<h3>Email<span class="subtitle">info@nathap.in</span></h3>
							</div>
						</div>

						<div class="col-md-3 col-sm-6 bottommargin clearfix">
							<div class="feature-box fbox-center fbox-bg fbox-plain">
								<div class="fbox-icon">
									<a href="https://www.facebook.com/NatHapTech/" target="_blank"><i class="icon-facebook"></i></a>
								</div>
								<h3>Follow on Facebook<span class="subtitle"> /NatHapTech</span></h3>
							</div>
						</div>

					</div><!-- Contact Info End -->

				</div>

			</div>

		</section><!-- #content end -->
 <script>
      function initMap() {
        var uluru = {lat: 26.846694, lng: 80.946166};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 10,
          center: uluru
        });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map
        });
      }
    </script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCMuTcktKGDMnBU1BkNcJNWI7WzxqjWm-4&callback=initMap"
  type="text/javascript"></script>		
<%@include file="footer.jsp"%>
<%@include file="webend.jsp"%>