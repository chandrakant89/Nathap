<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

    <%@include file="webResource.jsp"%>
        <%@include file="header.jsp"%>

    <section id="content">
        <div class="" style="margin-top: 60px;">
            <div class="container clearfix">
                <div class="heading-block center nomargin">
                    <h3>Our Services</h3>
                </div>
            </div>
        </div>
        <div class="container clearfix">

            <div class="col_one_fourth">
                <div class="text-center">
                    <img src="images/services/mall.jpg" class="img-circle" width="200" height="200"/>
                </div>
                <div class="feature-box fbox-small fbox-plain">
                    <div class="fbox-icon">
                        <a href="#">
                            <i class="icon-tablet2"></i>   
                        </a>
                    </div>
                    <h3>Micro-Mall</h3>
                    <p>Powerful Layout with Responsive functionality that can be adapted to any screen size.</p>
                </div>
            </div>

            <div class="col_one_fourth">
                <div class="text-center">
                    <img src="images/services/petrol.jpg" class="img-circle" width="200" height="200"/>
                </div>
                <div class="feature-box fbox-small fbox-plain">
                    <div class="fbox-icon">
                        <a href="#">
                            <i class="icon-beer"></i>
                        </a>
                    </div>
                    <h3>Oli/Gas Pump</h3>
                    <p>Looks beautiful &amp; ultra-sharp on Retina Displays with Retina Icons, Fonts &amp; Images.</p>
                </div>
            </div>

            <div class="col_one_fourth">
                <div class="text-center">
                    <img src="images/services/it.jpg" class="img-circle" width="200" height="200"/>
                </div>
                <div class="feature-box fbox-small fbox-plain">
                    <div class="fbox-icon">
                        <a href="#">
                            <i class="icon-line-monitor"></i>
                        </a>
                    </div>
                    <h3>ITES Services</h3>
                    <p>Optimized code that are completely customizable and deliver unmatched fast performance.</p>
                </div>
            </div>
            <div class="col_one_fourth col_last">
                <div class="text-center">
                    <img src="images/services/g2c.jpg" class="img-circle" width="200" height="200"/>
                </div>
                <div class="feature-box fbox-small fbox-plain">
                    <div class="fbox-icon">
                        <a href="#">
                            <i class="icon-globe"></i>
                        </a>
                    </div>
                    <h3>G2C Services</h3>
                    <p>Nathap provides support for Native HTML5 Videos that can be added to a Full Width Background.</p>
                </div>
            </div>

            <div class="col_one_fourth">
                <div class="text-center">
                    <img src="images/services/shop.jpg" class="img-circle" width="200" height="200"/>
                </div>
                <div class="feature-box fbox-small fbox-plain">
                    <div class="fbox-icon">
                        <a href="#">
                            <i class="icon-cart"></i>
                        </a>
                    </div>
                    <h3>E-commerce</h3>
                    <p>Display your Content attractively using Parallax Sections that have unlimited customizable areas.</p>
                </div>
            </div>

            <div class="col_one_fourth">
                <div class="text-center">
                    <img src="images/services/agro.jpg" class="img-circle" width="200" height="200"/>
                </div>
                <div class="feature-box fbox-small fbox-plain">
                    <div class="fbox-icon">
                        <a href="#">
                            <i class="icon-leaf"></i>
                        </a>
                    </div>
                    <h3>Agro</h3>
                    <p>Complete control on each &amp; every element that provides endless customization possibilities.</p>
                </div>
            </div>

            <div class="clear"></div>

            <!-- <div class="col_one_fourth bottommargin-sm">
                <div class="feature-box fbox-small fbox-plain">
                    <div class="fbox-icon">
                        <a href="#">
                            <i class="icon-book"></i>
                        </a>
                    </div>
                    <h3>Education</h3>
                    <p>Change your Website's Primary Scheme instantly by simply adding the dark class to the body.</p>
                </div>
            </div> -->

            <div class="clear"></div>

        </div>

    </section>
    <!-- #content end -->

    <%@include file="footer.jsp"%>
        <%@include file="webend.jsp"%>