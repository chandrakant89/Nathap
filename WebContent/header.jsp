<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

	<div id="top-bar">
		<div class="container clearfix">
			<div class="col_half nobottommargin">
				<div class="top-links">
					<ul>
						<li><a href="https://nathap.in">Home</a></li>
						<li><a href="contact.jsp">Contact</a></li>
					</ul>
				</div><!-- .top-links end -->
			</div>

			<div class="col_half fright col_last nobottommargin">
				<div id="top-social">
					<ul>
						<li><a href="#" class="si-facebook"><span class="ts-icon"><i class="icon-facebook"></i></span><span class="ts-text">Facebook</span></a></li>
						<li><a href="tel:+91 9125165305" class="si-call"><span class="ts-icon"><i class="icon-call"></i></span><span class="ts-text">(+91) 9125165305</span></a></li>
						<li><a href="mailto:info@nathap.in" class="si-email3"><span class="ts-icon"><i class="icon-email3"></i></span><span class="ts-text">info@Nathap.in</span></a></li>
					</ul>
				</div><!-- #top-social end -->
			</div>
		</div>
	</div><!-- #top-bar end -->
	<header id="header">
		<div id="header-wrap">
			<div class="container clearfix">
				<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>
				<div id="logo">
					<a href="https://nathap.in" class="standard-logo" data-dark-logo="images/logo.png"><img src="images/logo.png" alt=""></a>
					<a href="https://nathap.in" class="retina-logo" data-dark-logo="images/logo.png"><img src="images/logo.png" alt="Nathap Logo"></a>
				</div><!-- #logo end -->

				<nav id="primary-menu" class="sub-title">
					<ul>
						<li class="" id="homeId"><a href="https://nathap.in/"><div>Home</div></a></li>
						<li class="" id="aboutId"><a href="about.jsp"><div>About Us</div></a></li>
						<li class="" id="servId"><a href="service.jsp"><div>Services</div></a></li>
						<li class="" id="careerId"><a href="jobListServlet"><div>Career</div></a></li>
						<li class="" id="contId"><a href="contact.jsp"><div>Contact Us</div></a></li>
					</ul>
				</nav><!-- #primary-menu end -->
			</div>
		</div>
	</header><!-- #header end -->