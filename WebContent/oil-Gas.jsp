<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@include file="webResource.jsp"%>
<%@include file="header.jsp"%>

<section id="page-title" class="page-title-parallax page-title-dark"
	style="background-image: url('images/about/4.jpg'); padding: 120px 0; background-position: center; background-size: cover;"
	data-stellar-background-ratio="0.3">

	<div class="container clearfix">
		<h1>Oil-gas pump</h1>
		<span>Offer for Installation & Erection of Fuel Retail Outlet (RFO) with Auto LPG, MS (Petrol) & HSD (Diesel) Dealership </span>
	</div>

</section>

<section id="content">
			<div class="content-wrap">
				<div class="container clearfix">
					<div class="row clearfix">
						<div class="col-sm-12">
							<div class="row clearfix">
								<div class="">
									<div class="tabs tabs-alt clearfix" id="tabs-profile">
										<ul class="tab-nav clearfix">
											<li><a href="#tab-1"><i class="icon-rss2"></i> Key Equipment of RFO</a></li>
											<li><a href="#tab-2"><i class="icon-pencil2"></i> Scope Of Work</a></li>
											<li><a href="#tab-3"><i class="icon-reply"></i> Client</a></li>
											<li><a href="#tab-4"><i class="icon-users"></i> Investment</a></li>
											<li><a href="#tab-5"><i class="icon-users"></i> Terms Of Business</a></li>
										</ul>

										<div class="tab-container">
											<div class="tab-content clearfix" id="tab-1">
												<div class="col-sm-8">
													<h3>Key Equipment of RFO</h3>
													<p class="">This offer covers the supply, installation and commissioning of Retail Fuel Outlet. The supply consists of the following:</p>
													<div class="table-responsive">
														<table class="table table-bordered table-striped">
														 <tbody>
															<tr><td>MS, HSD & LPG Tank (20kl capacity each)</td></tr>
															<tr><td>Unloading Pumps</td></tr>
															<tr><td>Dispensing Pumps</td></tr>
															<tr><td>Tank Gauging Systems</td></tr>
															<tr><td>MS, HSD, Auto LPG Dispensers</td></tr>
															<tr><td>Excess Flow Check Valves</td></tr>
															<tr><td>Fire Safe Isolation Valves</td></tr>
															<tr><td>Remote Operated Valves</td></tr>
															<tr><td>Piping and Pipe Fittings</td></tr>
															<tr><td>Electrical & Instrumentation Panel</td></tr>
														  </tbody>
														</table>
													</div>
												</div>
												<div>
													<div class="col-sm-4">
														<h3>Design Standards</h3>
														<p>The installation will be done as per SMPV and Task force Guidelines laid by Chief Controller of Explosives and as under The Petroleum Act. It will abide to all other standards applicable for such installations.</p>
													</div>
												</div>

											</div>
											<div class="tab-content clearfix" id="tab-2">
												<div>
													<h3>Principal Company</h3>
												</div>
												
												<div class="table-responsive">
													<table class="table table-bordered table-striped">
													 <tbody>
														<tr><td>Co-ordination in selection of site, Design of Layout Plan, Preparation of detail drawing</td></tr>
														<tr><td>Design and preparation of detailed Engineering drawing, Schematic diagram, P & I diagrams, piping and equipment layout for installation and erection work.</td></tr>
														<tr><td>Approval of layout plan from Chief Controller of Explosives (PESO), Nagpur / Mumbai and obtaining approval for Construction & Erection.</td></tr>
														<tr><td>Designing of Detailed Engineering drawing for Civil Construction work.</td></tr>
														<tr><td>Supply of material and Erection & installation of the RFO.</td></tr>
														<tr><td>Safety Certification of Installation under Rule 33 from Competent Authority as per the PESO requirement.</td></tr>
														<tr><td>Preparation of License document for submission to the PESO and obtaining license for Testing and license for Operation.</td></tr>
														<tr><td>Arranging Third Party Inspection as required under rules</td></tr>
														<tr><td>Preparation of final layout plan, submission thereof to Chief Controller of Explosives, Nagpur and obtaining license for testing & operation.</td></tr>
														<tr><td>Arranging Inspection by designated officials from the Explosives departments, immediately after installation.</td></tr>
														<tr><td>Provide the type approval for the dispenser from the Department of Legal Metrology and Assistance in obtaining Local Weight and Measures Approval.</td></tr>
														<tr><td>Testing & Commissioning Of Dispenser.</td></tr>
														<tr><td>Commissioning the all other facilities.</td></tr>
														<tr><td>Training to the client’s personnel for effective management of the RFO.</td></tr>
														<tr><td>Logistics of Petrol, Diesel & Auto LPG to RFO.</td></tr>
													  </tbody>
													</table>
												</div>


											</div>
											<div class="tab-content clearfix" id="tab-3">
											<h3>Client</h3>
											<p>To obtain shop act license, make available Non-Agriculture certificate, obtain NOC from the local authority (District magistrate-DM/Collector in case of out of city site or municipal Commissioner in case of within city site). All such NOC and allied approvals from local authorities other than PESO and the director of metrology (for type approval of the dispenser) are in the scope of the client.</p>
											<p>Assigning a local coordinator to assist company team for all trouble shooting and resource mobilization.</p>
											<p>Civil work: You may note that company scope of supply is limited to the extent of technical supply, installation and commissioning. Following activities are not covered in the scope of company and it is the dealers responsibility to complete the following works:</p>
												<div class="table-responsive">
													<table class="table table-bordered table-striped">
									                    <tbody>
									                      <tr>
									                        <td>Foundation of the tank</td>
									                        <td>Foundation of the underground tanks as per the drawing submitted by the company</td>
									                      </tr>
									                      <tr>
									                        <td>Pipe support</td>
									                        <td>All civil works required for pipe supporting pipe</td>
									                      </tr>
									                      <tr>
									                        <td>Pipe Trenches</td>
									                        <td>Civil works pertaining to trenches required for below ground laid pipeline</td>
									                      </tr>
									                      <tr>
									                        <td>Fencing</td>
									                        <td>Civil works for erecting fencing around Tank storage area</td>
									                      </tr>
									                      <tr>
									                        <td>Trench & grouting pipes</td>
									                        <td>Civil work as applicable</td>
									                      </tr>
									                      <tr>
									                        <td>Flooring</td>
									                        <td>The piping and instrumentation area including the storage tank area should be covered with concrete interconnecting pavement blocks, popularly known as IPV. The other floor area may preferably be built with the same material or else done with concrete</td>
									                      </tr>
									                      <tr>
									                        <td>Excavation</td>
									                        <td>All excavation work is to be done by client</td>
									                      </tr>
									                      <tr>
									                        <td>Sand Filling</td>
									                        <td>In the trenches and underground tank pit</td>
									                      </tr>
									                        <tr>
									                        <td>Protective walls</td>
									                        <td>Depending on the location and surrounding locality, PESO may demand to erect a protective wall/s on either or all sides of the RFO. This expense is not considered in the quotation submitted. The wall needs to be erected as per CCOE specifications and demand by the civil contractor appointed by the client</td>
									                      </tr>
									                        <tr>
									                        <td>Lighting</td>
									                        <td>Minimum 6 poles of 7 meters height are required to light up the RFO</td>
									                      </tr>
									                      
									                    </tbody>
								                  </table>
												</div>
												<p>Infrastructure- Various kind of infrastructure is required to be provided by client on site. The details are:</p>
												<div class="table-responsive">
													<table class="table table-bordered table-striped">
									                    <tbody>
									                      <tr>
									                        <td>Control cabin</td>
									                        <td>To locate the control panel of the RFO</td>
									                      </tr>
									                      <tr>
									                        <td>Supply of electricity</td>
									                        <td>The total power required for the RFO is 20 IIP minimum. However, the power should be from a constant voltage stable source preferable with a dependable back-up. The client is expected to purchase a DG set for the RFO</td>
									                      </tr>
									                      <tr>
									                        <td>Availability of water</td>
									                        <td>Water is not required for operation of the RFO. However water should be made available during civil work and installation for hydro testing of pipes and various miscellaneous functions.</td>
									                      </tr>
									                      <tr>
									                        <td>Security</td>
									                        <td>Once the site work is on, most of the materials/equipment at various stages of installation could remain lying in the site for days and night. Client should provide security on the site to prevent any theft, and accidents</td>
									                      </tr>
									                    </tbody>
								                  </table>
												</div>
												<div class="table-responsive">
													<table class="table table-bordered table-striped">
									                    <tbody>
									                      <tr>
									                        <td>Company assures client to assist in every activity above to provide technical help only</td>
									                      </tr>
									                      <tr>
									                        <td>Calibration & Certification of local weight & measures department for the Dispensers</td>
									                      </tr>
									                      <tr>
									                        <td>All the required official payments of fees for obtaining approvals license, etc shall be paid by you as and when required</td>
									                      </tr>
									                      <tr>
									                        <td>Procurement of complete civil work materials and carrying out civil construction work, including foundation for plants and equipments</td>
									                      </tr>
									                       <tr>
									                        <td>Obtaining of soil bearing capacity from authorized lab</td>
									                      </tr>
									                    </tbody>
								                  </table>
												</div>
												
											</div>
											<div class="tab-content clearfix" id="tab-4">
												<h3>Investment</h3>
												<div class="table-responsive">
													<table class="table table-bordered table-striped">
									                    <thead>
									                      <tr>
									                        <th>S.N.</th>
									                        <th>Description of Item</th>
									                        <th>Price of complete set</th>
									                      </tr>
									                    </thead>
									                    <tbody>
									                      <tr>
									                        <td>1</td>
									                        <td>Total Turnkey installations include erection and commissioning along with necessary equipment of Retail Fuel Outlet as per defined scope of work. (Includes Mechanical/Electrical Installation only)</td>
									                        <td>Rs.70-90 lakh (Seventy to Ninety Lakh only )(Duties &amp; Taxes as applicable)
									                        </td>
									                      </tr>
									                    </tbody>
								                  </table>
												</div>
											</div>
											
											<div class="tab-content clearfix" id="tab-5">
												<h3>Terms Of Business</h3>
												<div class="panel-group" id="accordion">
								                    <div class="panel panel-danger">
								                      <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapse2" style="cursor: pointer;">
								                        <h5 class="panel-title">
								                          <a>BASIS OF PRICE </a>
								                        </h5>
								                      </div>
								                      <div id="collapse2" class="panel-collapse collapse in" style="height: auto;">
								                        <div class="panel-body">
								                            <div class="table-responsive">
								                            	<table class="table table-bordered table-striped">
									                              <tbody>
									                            	<tr><td>The prices offered above have been considered on TURN KEY BASIS.</td></tr>
										                            <tr> <td>The price excludes Sales Tax and Service Tax</td></tr>
										                            <tr><td>Transportation will be at actual &amp; will be borne by the customer.</td></tr>
										                            <tr><td>Octroi charges, if applicable shall be payable by the Client as and when required.</td></tr>
										                          </tbody>
									                            </table>
								                            </div>
								                        </div>
								                      </div>
								                    </div>
								                    <div class="panel panel-success">
								                      <div class="panel-heading collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse1" style="cursor: pointer;">
								                        <h5 class="panel-title">
								                          <a>VALIDITY </a>
								                        </h5>
								                      </div>
								                      <div id="collapse1" class="panel-collapse collapse" style="height: 0px;">
								                        <div class="panel-body">The price quoted above shall be valid for your consideration for a period of 60 days from the date of our offer.</div>
								                      </div>
								                    </div>
								                    <div class="panel panel-info">
								                      <div class="panel-heading collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse3" style="cursor: pointer;">
								                        <h5 class="panel-title">
								                          <a>MODE OF PAYMENT </a>
								                        </h5>
								                      </div>
								                      <div id="collapse3" class="panel-collapse collapse" style="height: 0px;">
								                        <div class="panel-body">
								                            <div class="table-responsive">
								                            	<table class="table table-bordered table-striped">
								                            		<tbody>
								                            		    <tr><td>Rs. 5, 00,000/- allotment fees along with Application from and Land Documents and layout.</td></tr>
										                                <tr><td>50% payment to be made immediately for placing orders for the above mentioned equipment.</td></tr>
										                                <tr><td>30% against Performa Invoice before Dispatch of Tanks and Equipment.</td></tr>
										                                <tr><td>Balance prior to mechanical installation.</td></tr>
										                            </tbody>
								                            	</table>
								                            </div>
								                        </div>
								                      </div>
								                    </div>
								                    <div class="panel panel-success">
								                      <div class="panel-heading collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse4" style="cursor: pointer;">
								                        <h5 class="panel-title">
								                          <a>SCHEDULE OF IMPLEMENTION </a>
								                        </h5>
								                      </div>
								                      <div id="collapse4" class="panel-collapse collapse" style="height: 0px;">
								                        <div class="panel-body">
								                            <p>We propose to complete the entire installation work within 6 weeks subject to the payment being made on time and standard force majeure clause applicable as well as the following provision:</p>
								                            <div class="table-responsive">
								                            	<table class="table table-bordered table-striped">
								                            		<tbody>
								                            			<tr><td>Monsoon period is to be excluded.</td></tr>
										                                <tr><td>The layout designs are approved and received within10 days from your side.</td></tr>
										                                <tr><td>Prompt furnishing of work fronts like Water, Electricity and local approvals in your scope.</td></tr>
										                                <tr><td>Prompt payment as per schedule on submission of bill.</td></tr>
										                                <tr><td>N.O.C. from District Magistrate / Collector is obtained in time.</td></tr>
								                            		</tbody>
								                            	</table>
								                            </div>  
								                        </div>
								                      </div>
								                    </div>
								                  </div>
											  </div>

										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>

			</div>

		</section><!-- #content end -->

<%@include file="footer.jsp"%>
<%@include file="webend.jsp"%>