<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@include file="webResource.jsp"%>
<%@include file="header.jsp"%>

<section id="content">

	<div class="content-wrap">

		<div class="container clearfix">

			<div class="heading-block title-center nobottomborder">
				<h1>We are currently Under Construction</h1>
				<span>Please check back again within Some Days as We're
					Pretty Close</span>
			</div>
			<div id="countdown-ex1"
				class="countdown countdown-large divcenter bottommargin"
				style="max-width: 700px;"></div>
		</div>

	</div>

</section>
<!-- #content end -->

<%@include file="footer.jsp"%>
<%@include file="webend.jsp"%>