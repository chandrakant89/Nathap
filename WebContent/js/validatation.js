
function applyJob(val, num){
   document.getElementById("jTitle").innerHTML = val;
   document.getElementById("position").value= val;
   document.getElementById("jobcode").value= num;
   document.getElementById("jobcode1").innerHTML= num;
   return false;
}


function isNumberMob(evt) {
	 var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57)){
        	return false;
        }else{
        	return true;
		}
	}    
 
 function isNumber(evt) {
     var iKeyCode = (evt.which) ? evt.which : evt.keyCode
     if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57)){
    	 
         return false;
     }else{
         return true;    	 
     }

 }    

 
 $(document).ready(function() {
	  $('#file').on('change', function(evt) {
	   if (this.files != null){
	    var filesize = this.files[0].size;
	    if(filesize > 5242880){
	    	document.getElementById('fileError').innerHTML = "Please upload less then 5MB.";	
	    }else{
	    	document.getElementById('fileError').innerHTML = "";	
	    }
	   }
	    
	  });
	});


 function ValidateExtensionApplyJob() {
	
	 /*file Validation*/
	var allowedFiles = [ ".doc", ".docx", ".pdf" ];
	var file = document.getElementById("file");
	var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|')
			+ ")$");
	if (!regex.test(file.value.toLowerCase())) {
		document.getElementById('fileError').innerHTML = "Please upload file having extensions .doc , .docx , .pdf only.";
		return false;
	} else {
		document.getElementById('fileError').innerHTML = "";
		
		/*captcha validation*/
		var v = grecaptcha.getResponse();
		if (v.length == 0) {
			document.getElementById('captcha').innerHTML = "You can't leave Captcha Code empty";
			return false;
		} else {
			document.getElementById('captcha').innerHTML = "";
			return true;
		}
	}
}
 
