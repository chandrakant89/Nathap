<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@include file="webResource.jsp" %>
<%@include file="header.jsp" %>
		
		<section id="slider" class="slider-parallax swiper_wrapper full-screen clearfix slider-parallax-visible">

			<div class="swiper-container swiper-parent">
				<div class="swiper-wrapper">
					<div class="swiper-slide dark bg_cover">
						<div class="container clearfix">
							<div class="slider_section text-center">
							<div class="row">
								<div class="col-md-4 col-sm-4">
									<a href="http://mall.nathap.in/" target="_blank">
									<div class="card_bg cbga">
										<h3>Micro Mall</h3>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicind facere officia. Aspernatur</p>
									</div>
									</a>
								</div>
								<div class="col-md-4 col-sm-4">
								<a href="oil-Gas.jsp">
									<div class="card_bg cbgb">
										<h3>Oil/Gas Pump</h3>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicind facere officia. Aspernatur</p>
									</div>
									</a>
								</div>
								
								<div class="col-md-4 col-sm-4">
								<a href="http://awebbit.com/" target="_blank">
									<div class="card_bg cbgc">
										<h3>ITES Services</h3>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicind facere officia. Aspernatur</p>
									</div>
									</a>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4 col-sm-4">
								<a href="http://supply-mafia.com/" target="_blank">
									<div class="card_bg cbgd">
										<h3>E-commerce</h3>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicind facere officia. Aspernatur</p>
									</div>
									</a>
								</div>
								<div class="col-md-4 col-sm-4">
								<a href="http://www.thenatureworld.in/" target="_blank">
									<div class="card_bg cbge">
										<h3>G2C Services</h3>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicind facere officia. Aspernatur</p>
									</div>
									</a>
								</div>
								
								<div class="col-md-4 col-sm-4">
								<a href="agro.jsp">
									<div class="card_bg cbgf">
										<h3>Agro</h3>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicind facere officia. Aspernatur</p>
									</div>
									</a>
								</div>
							</div>
							</div>
						</div>
					</div>
				</div>
<!-- 				<div id="slider-arrow-left"><i class="icon-angle-left"></i></div>
				<div id="slider-arrow-right"><i class="icon-angle-right"></i></div>
				<div id="slide-number"><div id="slide-number-current"></div><span>/</span><div id="slide-number-total"></div></div> -->
			</div>

		</section>

		<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap">

				<div class="container clearfix">
					<div class="row clearfix">

						<div class="col-lg-12">
							<div class="heading-block fancy-title nobottomborder title-bottom-border">
								<h4>Welcome to Nathap Technology & Research</h4>
							</div>
							<p class="lead">"Nat Hap is an online solution for different
								kind of essential needs that we come across in our day to day to
								life. Basically, it is intended to make your life easy &
								luxurious. It enables people to pay their mobile bills online
								easily and conveniently. It also assists to get services
								associated with e-governance.</p>
						</div>

					<!-- 	<div class="col-lg-7">

							<div style="position: relative; margin-bottom: -60px;" class="ohidden" data-height-lg="426" data-height-md="567" data-height-sm="470" data-height-xs="287" data-height-xxs="183">
								<img src="images/services/main-fbrowser.png" style="position: absolute; top: 0; left: 0;" data-animate="fadeInUp" data-delay="100" alt="Chrome">
								<img src="images/services/main-fmobile.png" style="position: absolute; top: 0; left: 0;" data-animate="fadeInUp" data-delay="400" alt="iPad">
							</div>

						</div> -->

					</div>
				</div>

				<div class="section nobottommargin">
					<div class="container clear-bottommargin clearfix">

						<div class="row topmargin-sm clearfix">

							<div class="col-md-4 heading-block fancy-title nobottomborder title-bottom-border">
								
								<div class="heading-block nobottomborder" style="margin-bottom: 15px;">
									<!-- <span class="before-heading">Scalable on Devices.</span> -->
									<h4><i class="i-plain color i-large icon-line2-screen-desktop inline-block"></i> Vision-2020</h4>
								</div>
								<p>NATHAP is a unique group of skilled, full of excellence, enthusiastic, as well as energetic people, who are committed to setting up their talent towards developing our country’s people not only in India but all over the world. NAT HAP’s resolve is to enrich the talent of the masses and make them and progressive.</p>
							</div>

							<div class="col-md-4 heading-block fancy-title nobottomborder title-bottom-border">
								<div class="heading-block nobottomborder" style="margin-bottom: 15px;">
									<!-- <span class="before-heading">Smartly Coded &amp; Maintained.</span> -->
									<h4><i class="i-plain color i-large icon-line2-energy inline-block"></i> Worthy-Goal</h4>
								</div>
								<p>NAT HAP is moving forward towards advanced development whose purpose is to add end-person of the country to the latest technology in daily life, to generate employment, Availability of quality education and ensuring research on it, Research on Agriculture based comprehensive experimentation.</p>
							</div>

							<div class="col-md-4 heading-block fancy-title nobottomborder title-bottom-border">
								<div class="heading-block nobottomborder" style="margin-bottom: 15px;">
									<!-- <span class="before-heading">Flexible &amp; Customizable.</span> -->
									<h4><i class="i-plain color i-large icon-line2-equalizer inline-block"></i>Mission-2020</h4>
								</div>
								<p>According to Vision 2020, NAT HAP” has to reach the Block-level in every state of the country and implement all the fixed projects of the company, Under its diligence, dignity and talent image, till 2020, the company will remain committed to the development of each person directly and indirectly.</p>
							</div>
						</div>
					</div>
				</div>

				
				<div style="background-color: #fff; padding: 30px 0;">
				<div class="container clearfix">
					<div id="oc-clients" class="owl-carousel image-carousel carousel-widget" data-margin="60" data-loop="true" data-nav="false" data-autoplay="4000" data-pagi="false" data-items-xxs="2" data-items-xs="3" data-items-sm="4" data-items-md="5" data-items-lg="6">
						<div class="oc-item"><a href="#"><img src="images/clients/1.png" alt="Clients"></a></div>
						<div class="oc-item"><a href="#"><img src="images/clients/2.png" alt="Clients"></a></div>
						<div class="oc-item"><a href="#"><img src="images/clients/3.png" alt="Clients"></a></div>
						<div class="oc-item"><a href="#"><img src="images/clients/4.png" alt="Clients"></a></div>
						<div class="oc-item"><a href="#"><img src="images/clients/5.png" alt="Clients"></a></div>
					</div>
				</div>
				</div>
			</div>

		</section><!-- #content end -->
<%@include file="footer.jsp" %>
<%@include file="webend.jsp" %>